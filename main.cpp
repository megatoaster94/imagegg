#include <iostream>
#include <iomanip>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <omp.h>
#include <chrono>
#include <thread>

#define USAGE usage(argv[0])

void usage(char *program)
{
    std::cout << "Usage: " << program << " image [threads]" << std::endl;
    std::cout << "\timage: path to image" << std::endl;
    std::cout << "\tthread (optional): amount of threads" << std::endl;
}

cv::Mat grayscale_image(cv::Mat original_image, int threads)
{
    cv::Mat gray_image(original_image.size(), CV_8UC1);

    int rows = original_image.rows;
    int cols = original_image.cols;
#pragma omp parallel for collapse(2) num_threads(threads)
    for(size_t i = 0; i < rows; ++i)
    {
        for(size_t j = 0; j < cols; ++j)
        {
            cv::Vec3b intensity = original_image.at<cv::Vec3b>(i, j);

            int blue = intensity.val[0];
            int green = intensity.val[1];
            int red = intensity.val[2];

            gray_image.at<uchar>(i, j) = blue*0.0722 + green*0.7152 + red*0.2126;
        }
    }
    return gray_image;
}

cv::Mat gaussian_image(cv::Mat original_image, int threads)
{
    cv::Mat gauss_image(original_image.size(), CV_8UC1);

    /* start producing Gaussian filter kernel*/
    const double PI = 4.0*atan(1.0) ;
    double sigma = 2;

    const int kernel_radius = 5;
    float kernel_array[kernel_radius][kernel_radius];

    double total = 0;

    //calculate each relevant value to neighbour pixels and store it in 2d array
#pragma omp parallel for collapse(2) num_threads(threads)
    for(int row = 0; row < kernel_radius; ++row)
    {
        for(int col = 0; col < kernel_radius; ++col)
        {
            double value = (1/(2*PI*pow(sigma,2)))*exp(-(pow(row-kernel_radius/2,2)+pow(col-kernel_radius/2,2))/(2*pow(sigma,2)));

            kernel_array[row][col] = value;

            total += value;
        }
    }

    //Scale value in 2d array in to 1
#pragma omp parallel for collapse(2) num_threads(threads)
    for(int row = 0; row < kernel_radius; ++row)
    {
        for(int col = 0; col < kernel_radius; ++col)
        {
            kernel_array[row][col] = kernel_array[row][col]/total;
        }
    }

    /*End producing Gaussian filter kernel*/

    int verticle_image_bound = (kernel_radius-1)/2;
    int horizontal_image_bound = (kernel_radius-1)/2;

    //Assian Gaussian Blur value of the center point.Repeating this process for all other points through image
#pragma omp parallel for collapse(4) num_threads(threads)
    for(int row = verticle_image_bound; row < original_image.rows-verticle_image_bound; ++row)
    {
        for(int col = horizontal_image_bound; col < original_image.cols-horizontal_image_bound; ++col)
        {
            float value = 0.0;

            for(int kRow = 0; kRow < kernel_radius; ++kRow)
            {
                for(int kCol = 0; kCol<kernel_radius; ++kCol)
                {
                    //multiply pixel value with corresponding gaussian kernal value
                    float pixel = original_image.at<uchar>(kRow+row-verticle_image_bound,kCol+col-horizontal_image_bound)*kernel_array[kRow][kCol];
                    value += pixel;
                }
            }
            //assign new values to central point
            gauss_image.at<uchar>(row,col) = cvRound(value);
        }
    }

    return gauss_image;
}

void timer(std::chrono::time_point<std::chrono::high_resolution_clock> start, std::string message)
{
    const auto finish = std::chrono::high_resolution_clock::now();
    auto duration = finish - start;
    const auto min = std::chrono::duration_cast< std::chrono::minutes > ( duration );
    duration -= min;
    const auto sec = std::chrono::duration_cast< std::chrono::seconds > ( duration );
    duration -= sec;
    const auto milli = std::chrono::duration_cast< std::chrono::milliseconds > ( duration );
    std::cout << message
              << min.count() << " m "
              << sec.count() << "."
              << std::setw( 3 ) << std::setfill( '0' ) << milli.count() << " s\n";
}

int main(int argc, char *argv[])
{
    if (argc == 2 || argc == 3)
    {
        const auto start = std::chrono::high_resolution_clock::now();

        //int threads = std::thread::hardware_concurrency();
        int threads = omp_get_max_threads();
        //std::cout << "Number of threads: " << threads << std::endl;
        if (argc == 3)
            threads = atoi(argv[2]);
        if (threads == 0)
            threads = 1;

        cv::Mat rgb_image;
        rgb_image = cv::imread(argv[1]);

        if (rgb_image.empty())
        {
            std::cout << "Error loading image." << std::endl;
            return 1;
        }

        cv::Mat gray_image = grayscale_image(rgb_image, threads);

        imwrite("grayscale_image.jpg", gray_image);

        cv::Mat final_image = gaussian_image(gray_image, threads);

        imwrite("final_image.jpg", final_image);

        timer(start, "Time remaining: ");
    }
    else
    {
        USAGE;
        return 1;
    }

    return 0;
}
